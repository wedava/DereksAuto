from django.conf.urls import url, patterns
from .views import *
from django.contrib.auth.decorators import login_required

urlpatterns = patterns('',
                       url(r'^$', product_list, name='list'),
                       url(r'^product/add/$', login_required(ProductCreateView.as_view()), name='create'),
                       url(r'^product/update/(?P<id>[^/]+)/$', login_required(ProductUpdateView.as_view()),
                           name='update'),
                       url(r'^product/delete/(?P<pk>[^/]+)/$', login_required(ProductDeleteView.as_view()),
                           name='delete'),
                       # Commented out redirect due to AdWords
                       # url(r'^product/(?P<id>[^/]+)/$', ProductRedirectView.as_view(), name='redirect'),
                       url(r'^product/(?P<id>[^/]+)/$', ProductDetailView.as_view(), name='details'),
                       )
