# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_item_image_link'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='image_link',
            field=models.URLField(),
        ),
    ]
