# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20150624_1323'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='initial_price',
            field=models.FloatField(default=0.0),
        ),
        migrations.AlterField(
            model_name='item',
            name='amazon_url',
            field=models.URLField(blank=True),
        ),
    ]
