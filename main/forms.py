from django import forms
from .models import Item


class ItemForm(forms.ModelForm):
    image_link = forms.URLField(widget=forms.FileInput(), label="Image(250x250)")

    class Meta:
        model = Item
        fields = ['title', 'manufacturer', 'price', 'initial_price', 'shipping', 'amazon_url', 'image_link']


class ItemUpdateForm(forms.ModelForm):

    class Meta:
        model = Item
        fields = ['title', 'manufacturer', 'price', 'initial_price', 'shipping', 'amazon_url', 'image_link']

