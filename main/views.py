from django.views.generic import RedirectView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Item
from forms import ItemForm, ItemUpdateForm
from django.conf import settings
import tempfile
import os
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, render_to_response
from django.template.context import RequestContext
from django.core.exceptions import PermissionDenied
from django.contrib.messages.views import SuccessMessageMixin


def product_list(request):
    items = Item.objects.all()
    return render_to_response("main/product_list.html", locals(), context_instance=RequestContext(request))


class ProductDetailView(DetailView):
    model = Item
    template_name = 'main/product_details.html'

    def get_object(self, queryset=None):
        obj = get_object_or_404(Item, pk=self.kwargs['id'])
        return obj

    def get_context_data(self, **kwargs):
        context = super(ProductDetailView, self).get_context_data(**kwargs)
        return context


class ProductRedirectView(RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        url = Item.objects.get(id=kwargs['id']).amazon_url
        return url

    def get(self, request, *args, **kwargs):
        url = self.get_redirect_url(**kwargs)
        if url:
            return HttpResponseRedirect(url)
        # Below code never gets covered since image_link is required
        # To be used if image_link is made optional
        else:
            return reverse(ProductCreateView, kwargs=dict(id=kwargs['id']))


class ProductCreateView(SuccessMessageMixin, CreateView):
    model = Item
    template_name = 'main/product_add.html'
    form_class = ItemForm
    success_url = '/product/add/'
    success_message = 'Item added'

    def get_context_data(self, **kwargs):
        kwargs['items'] = Item.objects.all()
        return super(ProductCreateView, self).get_context_data(**kwargs)

    def form_valid(self, form):
        from imgurpython import ImgurClient
        client = ImgurClient(settings.CLIENT_ID, settings.CLIENT_SECRET)
        mem_file = self.request.FILES['image_link']
        tup = tempfile.mkstemp()
        f = os.fdopen(tup[0], 'w')
        f.write(mem_file.read())
        f.close()
        filepath = tup[1]
        upload = client.upload_from_path(filepath)
        form.instance.image_link = upload['link']
        return super(ProductCreateView, self).form_valid(form)


class ProductUpdateView(SuccessMessageMixin, UpdateView):
    model = Item
    # fields = ['title', 'image_link', 'manufacturer', 'price', 'shipping', 'amazon_url']
    template_name = 'main/product_update.html'
    form_class = ItemUpdateForm
    success_url = '/product/add/'
    success_message = 'Item updated'

    def get_object(self, queryset=None):
        obj = Item.objects.get(id=self.kwargs['id'])
        return obj


class ProductDeleteView(SuccessMessageMixin, DeleteView):
    model = Item
    success_url = '/product/add/'
    success_message = 'Item deleted'
    template_name = 'main/product_delete.html'

    def get_object(self, queryset=None):
        obj = Item.objects.get(id=self.kwargs['pk'])
        super(ProductDeleteView, self).get_object()
        if self.request.user.is_superuser:
            return obj
        else:
            raise PermissionDenied

    def delete(self, request, *args, **kwargs):
        return super(ProductDeleteView, self).delete(request, *args, **kwargs)



