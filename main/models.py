from django.db import models
import uuid

class Item(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=100)
    image_link = models.URLField()
    manufacturer = models.CharField(max_length=40)
    price = models.FloatField()
    initial_price = models.FloatField(default=0.00)
    shipping = models.FloatField(default=0.00)
    amazon_url = models.URLField(blank=True)

    def __str__(self):
        return self.title

    @property
    def offer(self):
        if self.initial_price > self.price:
            diff = self.initial_price - self.price
            return format((diff/self.initial_price)*100, '.1f')
        else:
            return 0

