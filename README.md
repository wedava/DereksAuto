# Dereks-Auto

This is a third party site for linking ads in Google shopping to DereksAuto storefront in Amazon.

The site has the following capabilities:

  - Addition, deletion and updating of items.
  - A product list page displaying all products in DereksAuto's storefront.
  - A product details page that also serves as the landing page from Google shopping ads.